# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [2.0.0]

-  Updated the supporting Guzzle library from 3.7 to 6.x
-  This bumped the required PHP version from 5.3 to 5.5