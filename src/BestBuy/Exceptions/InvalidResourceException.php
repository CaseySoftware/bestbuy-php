<?php

namespace BestBuy\Exceptions;

/**
 * Class InvalidResourceException
 * @package BestBuy\Exceptions
 *
 * @todo This should probably be renamed to 'InvalidResource' as it's already in the 'Exception' namespace.
 */
class InvalidResourceException extends \Exception
{

}